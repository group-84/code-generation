tos=1
tmp_no=0
accum=0
stack=[['TEMP',0] for i in range(10)]
types=['VAR','BINOP','UNIOP','TEMP']


def store(tmp_no,accum):
    tmp_no += 1
    print('ST','$'+ str(tmp_no));
    stack[accum][1] = tmp_no
    accum = 0
    return tmp_no


def push(typ,v,tos):
    stack[tos][0]=typ
    stack[tos][1]=v
    tos=tos+1
    return tos

def pop(tos,tmp_no):
    tos-=1
    if( stack[tos][0]=='TEMP' and stack[tos][1]!=0 ):
        tmp_no-=1

    return [tos,tmp_no]


def stackval(spos):
    
    if(stack[spos][0]=='TEMP'):
        s='$'+str(stack[spos][1])
    else:
        s=str(stack[spos][1])
    return s


# c=input() 
c= "AB+CD++"

ty='VAR'

for i in c:
    if(i.isalpha()):
        ty='VAR'
        tok=i.upper()
    elif(i=='@'):
        ty='UNIOP'
        tok=i
    else:
        ty='BINOP'
        if(i=='+'):
            tok='A' 
        elif(i=='-'):
            tok='S'
        elif(i=='*'):
            tok='M'
        else:
            tok='D'
    if(ty=='VAR'):
        tos=push('VAR',tok,tos)
    elif(ty=='UNIOP'):
        if(accum and accum!=tos-1):
            tmp_no = store(tmp_no,accum)
        if(accum):
            print('N')
        else:
            print('L',stackval(tos-1))
            print('N')
        pop(tos,tmp_no)
        accum=push('TEMP',0,tos)
    elif(ty=='BINOP'):
        if(accum and accum!=tos-1 and accum!=tos-2):  
            tmp_no = store(tmp_no,accum)
        if(accum==tos-2):
            print(tok,stackval(tos-1))
        elif(accum==tos-1):
            if(tok=='A' or tok=='M'):
                print(tok,stackval(tos-2))     
            else:
                if(tok=='S'):
                    print('N')
                    print('A',stackval(tos-2))
                else:
                    tmp_no = store(tmp_no,accum)
                    print('L',stackval(tos-2))
                    print('D',stackval(tos-1))
        else:
            print('L',stackval(tos-2))
            print(tok,stackval(tos-1))
        tos,tmp_no=pop(tos,tmp_no)
        tos,tmp_no=pop(tos,tmp_no)
        accum=push('TEMP',0,tos)-1
        tos+=1
