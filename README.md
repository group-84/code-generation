# Code generation
# SIC Machine Postfix Expression Translator

This project involves the creation of a backend for a translator that converts postfix arithmetic expressions into assembly language code for a Simplified Instructional Computer (SIC) machine. The SIC machine has a single register and supports various arithmetic and storage-related instructions. The translator takes a file containing postfix expressions as input and generates corresponding assembly code as output.

## Table of Contents
1 Introduction
2 Input
3 Output
4 Usage
5 Example
6 Contributing
7 License

## Introduction
The SIC Machine Postfix Expression Translator is designed to convert postfix arithmetic expressions into assembly language code suitable for execution on the SIC machine. The translator supports the following instructions:

- L: Load the operand into the register
- A: Add the operand to the contents of the register
- S: Subtract the operand from the contents of the register
- M: Multiply the contents of the register by the operand
- D: Divide the contents of the register by the operand
- N: Negate the contents of the register
- ST: Store the contents of the register in the operand location

## Input
The input file consists of several legitimate postfix expressions, each on a separate line. Expression operands are single letters, and operators are the normal arithmetic operators (`+`, `-`, `*`, `/`) and unary negation (`@`).

## Output
The output consists of assembly language code that meets the following requirements:

1. One instruction per line with the instruction mnemonic separated from the operand (if any) by one blank.
2. One blank line must separate the assembly code for successive expressions.
3. The original order of the operands must be preserved in the assembly code.
4. Assembly code must be generated for each operator as soon as it is encountered.
5. As few temporaries as possible should be used, given the restrictions.
6. For each operator in the expression, the minimum number of instructions must be generated, given the restrictions.

## Usage
To use the SIC Machine Postfix Expression Translator, follow these steps:

1. Clone the repository:
   git clone https://gitlab.com/group-84/code-generation

2. Navigate to the project directory:
   cd code-generation

3. Install any required dependencies (if applicable) and set up the environment.

4. Run the code script on the input file:
   python code.py input.txt output.txt

5. The translator will generate the corresponding assembly language code.

## Example
**Input (`input.txt`):**
AB+CD+EF++GH+++
AB+CD+-

**Output (`output.txt`):**
L A
A B
ST $1

L C
A D
ST $2
L E
A F
A $2
ST $2

L G
A H
A $2
A $1

L A
A B
ST $1
L C
A D
N
A $1

## Contributing
Contributions to this project are welcome. If you want to contribute, please follow the guidelines outlined in the README.md file.

## License
This project is licensed under the MIT License.


